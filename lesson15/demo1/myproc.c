#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>

int main()
{
    extern char **environ;
    pid_t id = fork();
    if(id == 0)
    {
        //child
        printf("我是子进程: %d\n", getpid());
        //execl: 如果替换成功，不会有返回值，如果替换失败，一定有返回值 ==》如果失败了，必定返回 ==》 只要有返回值，就失败了
        //不用对该函数进行返回值判断，只要继续向后运行一定是失败的！
        //execl("/bin/ls", "ls", "-a", "-ln", NULL); //lsssss: 不存在
        //char *const myargv[] = {
        //    "ls",
        //    "-a",
        //    "-l",
        //    "-n",
        //    NULL
        //};
        //execv("/bin/ls", myargv); //lsssss: 不存在
        //execlp("ls", "ls", "-a", "-l", "-n", NULL);
        //execvp("ls", myargv);
        //char *const myenv[] = {
        //    "MYENV=YouCanSeeMe",
        //    NULL
        //};
        //putenv("MYENV=YouCanSeeMe");
        //execle("./exec/otherproc", "otherproc", NULL, environ);
        //execl("./exec/shell.sh", "shell.sh", NULL);
        execl("./exec/test.py", "test.py", NULL);
        exit(1);
    }

    sleep(1);
    //father
    int status = 0;
    printf("我是父进程: %d\n", getpid());
    waitpid(id, &status, 0);
    printf("child exit code: %d\n", WEXITSTATUS(status));

    return 0;
}
