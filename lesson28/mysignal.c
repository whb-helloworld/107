#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <pthread.h>

void *thread1_run(void *args)
{
    while(1)
    {
        printf("我是线程1， 我正在运行\n");
        sleep(1);
    }
}

void *thread2_run(void *args)
{
    while(1)
    {
        printf("我是线程2， 我正在运行\n");
        sleep(1);
    }
}

void *thread3_run(void *args)
{
    while(1)
    {
        printf("我是线程3， 我正在运行\n");
        sleep(1);
    }
}

int main()
{
    pthread_t t1, t2,t3;
    pthread_create(&t1, NULL, thread1_run, NULL);
    pthread_create(&t2, NULL, thread2_run, NULL);
    pthread_create(&t3, NULL, thread3_run, NULL);


    while(1)
    {
        printf("我是主线程， 我正在运行\n");
        sleep(1);
    }

}


























// pid_t id;

// // 对吗？？
// void waitProcess(int signo)
// {
//     printf("捕捉到一个信号: %d, who: %d\n", signo, getpid());
//     sleep(5);

//     // 5个退出，5个没退
//     while (1)
//     {
//         pid_t res = waitpid(-1, NULL, WNOHANG);
//         if (res > 0)
//         {
//             printf("wait success, res: %d, id: %d\n", res, id);
//         }
//         else break; // 如果没有子进程了？
//     }

//     printf("handler done...\n");
// }

// int main()
// {
//     // signal(SIGCHLD, waitProcess);

//     signal(SIGCHLD, SIG_IGN);

//     int i = 1;
//     for (; i <= 10; i++)
//     {
//         id = fork();
//         if (id == 0)
//         {
//             // child
//             int cnt = 5;
//             while (cnt)
//             {
//                 printf("我是子进程, 我的pid: %d, ppid: %d\n", getpid(), getppid());
//                 sleep(1);
//                 cnt--;
//             }

//             exit(1);
//         }
//     }
//     // 如果你的父进程没有事干，你还是用以前的方法
//     // 如果你的父进程很忙，而且不退出，可以选择信号的方法
//     while (1)
//     {
//         sleep(1);
//     }

//     return 0;
// }

// volatile int quit = 0; // 保证内存可见性

// void handler(int signo)
// {
//     printf("change quit from 0 to 1\n");
//     quit = 1;
//     printf("quit : %d\n", quit);
// }

// int main()
// {
//     // signal(2, handler);

//     // while(!quit); //注意这里我们故意没有携带while的代码块，故意让编译器认为在main中，quit只会被检测

//     // printf("main quit 正常\n");

//     return 0;
// }