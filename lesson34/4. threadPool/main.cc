// #include "ThreadPool_V1.hpp"
// #include "ThreadPool_V2.hpp"
// #include "ThreadPool_V3.hpp"
#include "ThreadPool_V4.hpp"
#include "Task.hpp"
#include <memory>

// void callback(int result, int code)
// {
//     std::cut << ....
// }

int main()
{
    // std::unique_ptr<ThreadPool<Task>> tp(new ThreadPool<Task>(20));
    // tp->init();
    // tp->start();
    // tp->check(); 
    printf("0X%x\n", ThreadPool<Task>::getinstance());
    printf("0X%x\n", ThreadPool<Task>::getinstance());
    printf("0X%x\n", ThreadPool<Task>::getinstance());
    printf("0X%x\n", ThreadPool<Task>::getinstance());
    printf("0X%x\n", ThreadPool<Task>::getinstance());
    printf("0X%x\n", ThreadPool<Task>::getinstance());

    while (true)
    {
        int x, y;
        char op;
        std::cout << "please Enter x> ";
        std::cin >> x;
        std::cout << "please Enter y> ";
        std::cin >> y;
        std::cout << "please Enter op(+-*/%)> ";
        std::cin >> op;

        Task t(x, y, op);
        ThreadPool<Task>::getinstance()->pushTask(t); //单例对象也有可能在多线程场景中使用！
        // tp->pushTask(t);

        // 充当生产者, 从网络中读取数据，构建成为任务，推送给线程池
        // sleep(1);
        // tp->pushTask();
    }
}