#pragma once

#include <iostream>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

namespace ns_server
{
    enum{
        SOCKET_ERR=1,
        BIND_ERR
    };

    const static uint16_t default_port = 8080;

    class UdpServer
    {
    public:
        UdpServer(std::string ip, uint16_t port = default_port): ip_(ip), port_(port)
        {
            std::cout << "server addr: " << ip << " : " << port_ << std::endl;
        }
        void InitServer()
        {
            // 1. 创建socket接口，打开网络文件
            sock_ = socket(AF_INET, SOCK_DGRAM, 0);
            if(sock_ < 0)
            {
                std::cerr << "create socket error: " << strerror(errno) << std::endl;
                exit(SOCKET_ERR);
            }
            std::cout << "create socket success: " << sock_ << std::endl; //3

            // 2. 给服务器指明IP地址(??)和Port
            struct sockaddr_in local;  // 这个 local 在哪里定义呢？用户空间的特定函数的栈帧上，不在内核中！
            bzero(&local, sizeof(local));

            local.sin_family = AF_INET; //PF_INET
            local.sin_port = htons(port_);
            // inet_addr: 1,2
            // 1. 字符串风格的IP地址，转换成为4字节int, "1.1.1.1" -> uint32_t -> 能不能强制类型转换呢？不能，这里要转化
            // 2. 需要将主机序列转化成为网络序列
            local.sin_addr.s_addr = inet_addr(ip_.c_str());
            if(bind(sock_, (struct sockaddr*)&local, sizeof(local)) < 0)
            {
                std::cerr << "bind socket error: " << strerror(errno) << std::endl;
                exit(BIND_ERR);
            }
            std::cout << "bind socket success: " << sock_ << std::endl; //3
        }
        void Start()
        {
        }
        ~UdpServer() {}

    private:
        int sock_;
        uint16_t port_;
        std::string ip_; // 后面要专门研究一下，后面要去掉这个ip
    };
} // end NS_SERVER