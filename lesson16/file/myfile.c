#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



int main()
{
    //C库
    fprintf(stdout, "hello fprintf\n");
    //系统调用
    const char *msg = "hello write\n";
    write(1, msg, strlen(msg)); //+1?

    //fork(); //????
    return 0;
}






//#define LOG_NORMAL "logNormal.txt"
//#define LOG_ERROR "logError.txt"
//
//// 系统方案
//int main()
//{
//    int fd = open(LOG_NORMAL, O_CREAT|O_WRONLY|O_APPEND, 0666);
//    if(fd < 0)
//    {
//        perror("open");
//        return 1;
//    }
//
//    dup2(fd, 1);
//
//
//    printf("hello world, hello bit!\n");
//
//    close(fd);
//
//
////    printf();
////    perror();
////    cout<<
////    cerr<<
////    close(1);
////    open(LOG_NORMAL, O_WRONLY | O_CREAT | O_APPEND, 0666);
////
////    close(2);
////    open(LOG_ERROR, O_WRONLY | O_CREAT | O_APPEND, 0666);
////
////
////    //因为Linux下一切皆文件，所以，向显示器打印，本质就是向文件中写入， 如何理解？TODO
////    //C
////    printf("hello printf->stdout\n");
////    printf("hello printf->stdout\n");
////    printf("hello printf->stdout\n");
////    printf("hello printf->stdout\n");
////    fprintf(stdout, "hello fprintf->stdout\n");
////    fprintf(stdout, "hello fprintf->stdout\n");
////    fprintf(stdout, "hello fprintf->stdout\n");
////    fprintf(stdout, "hello fprintf->stdout\n");
////    fprintf(stdout, "hello fprintf->stdout\n");
////    fprintf(stdout, "hello fprintf->stdout\n");
////    fprintf(stdout, "hello fprintf->stdout\n");
////    fprintf(stdout, "hello fprintf->stdout\n");
////
////    fprintf(stderr, "hello fprintf->stderr\n");
////    fprintf(stderr, "hello fprintf->stderr\n");
////    fprintf(stderr, "hello fprintf->stderr\n");
////    fprintf(stderr, "hello fprintf->stderr\n");
////    fprintf(stderr, "hello fprintf->stderr\n");
////    fprintf(stderr, "hello fprintf->stderr\n");
////    fprintf(stderr, "hello fprintf->stderr\n");
////    fprintf(stderr, "hello fprintf->stderr\n");
////    //printf("%d\n", stdin->_fileno);
//    //printf("%d\n", stdout->_fileno);
//    //printf("%d\n", stderr->_fileno);
//    //FILE *fp = fopen(LOG, "w");
//
//    //printf("%d\n", fp->_fileno);
//    
//    //fclose(stdin); //close(0);
//    //close(0);
//    //close(2);
//    //close(0);
//    //int fd = open(LOG, O_WRONLY | O_CREAT | O_TRUNC, 0666);
//    //close(1);
//    //int fd = open(LOG, O_WRONLY | O_CREAT | O_APPEND, 0666);
//    ////int fd = open(LOG, O_RDONLY); //fd = 0;
//
//    ////int a, b;
//    ////scanf("%d %d", &a, &b);
//
//    ////printf("a = %d, b = %d\n", a, b);
//
//    //printf("you can see me !\n"); //stdout -> 1
//    //printf("you can see me !\n");
//    //printf("you can see me !\n");
//    //printf("you can see me !\n");
//    //printf("you can see me !\n");
//    //printf("you can see me !\n");
//    //printf("you can see me !\n");
//
//
//    //int fd1 = open(LOG, O_WRONLY | O_CREAT | O_TRUNC, 0666);
//    //int fd2 = open(LOG, O_WRONLY | O_CREAT | O_TRUNC, 0666);
//    //int fd3 = open(LOG, O_WRONLY | O_CREAT | O_TRUNC, 0666);
//    //int fd4 = open(LOG, O_WRONLY | O_CREAT | O_TRUNC, 0666);
//    //int fd5 = open(LOG, O_WRONLY | O_CREAT | O_TRUNC, 0666);
//    //int fd6 = open(LOG, O_WRONLY | O_CREAT | O_TRUNC, 0666);
//
//    //printf("%d\n", fd1);
//    //printf("%d\n", fd2);
//    //printf("%d\n", fd3);
//    //printf("%d\n", fd4);
//    //printf("%d\n", fd5);
//    //printf("%d\n", fd6);
//
//
//
//    //fopen(LOG, "w");
//    //fopen(LOG, "a");
//    //umask(0);
//    //// O_CREAT|O_WRONLY: 默认不会对原始文件内容做清空
//    ////int fd = open(LOG, O_WRONLY | O_CREAT, 0666);
//    ////int fd = open(LOG, O_WRONLY | O_CREAT | O_TRUNC, 0666);
//    ////int fd = open(LOG, O_WRONLY | O_CREAT | O_APPEND, 0666); //??
//    //int fd = open(LOG, O_RDONLY); //??
//    //if(fd == -1)
//    //{
//    //    printf("fd: %d, errno: %d, errstring: %s\n", fd, errno, strerror(errno));
//    //}
//    //else printf("fd: %d, errno: %d, errstring: %s\n", fd, errno, strerror(errno));
//
//    //char buffer[1024];
//    //// 这里我们无法做到按行读取，我们是整体读取的。
//    //ssize_t n = read(fd, buffer, sizeof(buffer)-1); //使用系统接口来进行IO的时候，一定要注意，\0问题
//    //if(n > 0)
//    //{
//    //    buffer[n] = '\0';
//
//    //    printf("%s\n", buffer);
//    //}
//
//    //C语言 , 和我们今天讲的接口， 关系？库函数 底层 调用 系统调用
//   // const char *msg = "bbb";
//   // int cnt = 1;
//
//   // while(cnt)
//   // {
//   //     char line[128];
//   //     snprintf(line, sizeof(line), "%s, %d\n", msg, cnt);
//
//   //     write(fd, line, strlen(line)); //这里的strlen不要+1, \0是C语言的规定，不是文件的规定！
//   //     cnt--;
//   // }
//   // 
//
//   // close(fd);
//
//    return 0;
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//// 语言方案
////#include <stdio.h>
////
////#define LOG "log.txt"
////
////
////int main()
////{
////    // w: 默认写方式打开文件，如果文件不存在，就创建它
////    // 1. 默认如果只是打开，文件内容会自动被清空
////    // 2. 同时，每次进行写入的时候，都会从最开始进行写入
////    //FILE *fp = fopen(LOG, "w");
////    // a: 不会清空文件，而是每一次写入都是从文件结尾写入的， 追加
////    // FILE *fp = fopen(LOG, "a");
////    FILE *fp = fopen(LOG, "r");
////    if(fp == NULL)
////    {
////        perror("fopen"); //fopen: XXXX
////        return 1;
////    }
////
////    //正常进行文件操作
////    while(1)
////    {
////        char line[128];
////        if(fgets(line, sizeof(line), fp) == NULL) break;
////        else printf("%s", line);
////    }
////    //const char *msg = "aaa\n";
////    //int cnt = 1;
////    //while(cnt)
////    //{
////    //    fputs(msg, fp);
////    //    //char buffer[256];
////    //    //snprintf(buffer, sizeof(buffer), "%s:%d:whb\n", msg, cnt);
////
////    //    //fputs(buffer, fp);
////    //    //printf("%s", buffer);
////    //    //fprintf(fp, "%s: %d: whb\n", msg, cnt);
////    //    //fprintf(stdout, "%s: %d: whb\n", msg, cnt); // Linux一切皆文件，stdout也对应一个文件, 显示器文件
////    //    //fputs(msg, fp);
////    //    cnt--;
////    //}
////
////    fclose(fp);
////    return 0;
////}
