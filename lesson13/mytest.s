
mytest:     file format elf64-x86-64


Disassembly of section .init:

0000000000401000 <_init>:
  401000:	48 83 ec 08          	sub    $0x8,%rsp
  401004:	48 8b 05 ed 2f 00 00 	mov    0x2fed(%rip),%rax        # 403ff8 <__gmon_start__>
  40100b:	48 85 c0             	test   %rax,%rax
  40100e:	74 05                	je     401015 <_init+0x15>
  401010:	e8 5b 00 00 00       	callq  401070 <__gmon_start__@plt>
  401015:	48 83 c4 08          	add    $0x8,%rsp
  401019:	c3                   	retq   

Disassembly of section .plt:

0000000000401020 <.plt>:
  401020:	ff 35 e2 2f 00 00    	pushq  0x2fe2(%rip)        # 404008 <_GLOBAL_OFFSET_TABLE_+0x8>
  401026:	ff 25 e4 2f 00 00    	jmpq   *0x2fe4(%rip)        # 404010 <_GLOBAL_OFFSET_TABLE_+0x10>
  40102c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000401030 <getpid@plt>:
  401030:	ff 25 e2 2f 00 00    	jmpq   *0x2fe2(%rip)        # 404018 <getpid@GLIBC_2.2.5>
  401036:	68 00 00 00 00       	pushq  $0x0
  40103b:	e9 e0 ff ff ff       	jmpq   401020 <.plt>

0000000000401040 <printf@plt>:
  401040:	ff 25 da 2f 00 00    	jmpq   *0x2fda(%rip)        # 404020 <printf@GLIBC_2.2.5>
  401046:	68 01 00 00 00       	pushq  $0x1
  40104b:	e9 d0 ff ff ff       	jmpq   401020 <.plt>

0000000000401050 <__assert_fail@plt>:
  401050:	ff 25 d2 2f 00 00    	jmpq   *0x2fd2(%rip)        # 404028 <__assert_fail@GLIBC_2.2.5>
  401056:	68 02 00 00 00       	pushq  $0x2
  40105b:	e9 c0 ff ff ff       	jmpq   401020 <.plt>

0000000000401060 <__libc_start_main@plt>:
  401060:	ff 25 ca 2f 00 00    	jmpq   *0x2fca(%rip)        # 404030 <__libc_start_main@GLIBC_2.2.5>
  401066:	68 03 00 00 00       	pushq  $0x3
  40106b:	e9 b0 ff ff ff       	jmpq   401020 <.plt>

0000000000401070 <__gmon_start__@plt>:
  401070:	ff 25 c2 2f 00 00    	jmpq   *0x2fc2(%rip)        # 404038 <__gmon_start__>
  401076:	68 04 00 00 00       	pushq  $0x4
  40107b:	e9 a0 ff ff ff       	jmpq   401020 <.plt>

0000000000401080 <getppid@plt>:
  401080:	ff 25 ba 2f 00 00    	jmpq   *0x2fba(%rip)        # 404040 <getppid@GLIBC_2.2.5>
  401086:	68 05 00 00 00       	pushq  $0x5
  40108b:	e9 90 ff ff ff       	jmpq   401020 <.plt>

0000000000401090 <sleep@plt>:
  401090:	ff 25 b2 2f 00 00    	jmpq   *0x2fb2(%rip)        # 404048 <sleep@GLIBC_2.2.5>
  401096:	68 06 00 00 00       	pushq  $0x6
  40109b:	e9 80 ff ff ff       	jmpq   401020 <.plt>

00000000004010a0 <fork@plt>:
  4010a0:	ff 25 aa 2f 00 00    	jmpq   *0x2faa(%rip)        # 404050 <fork@GLIBC_2.2.5>
  4010a6:	68 07 00 00 00       	pushq  $0x7
  4010ab:	e9 70 ff ff ff       	jmpq   401020 <.plt>

Disassembly of section .text:

00000000004010b0 <_start>:
  4010b0:	31 ed                	xor    %ebp,%ebp
  4010b2:	49 89 d1             	mov    %rdx,%r9
  4010b5:	5e                   	pop    %rsi
  4010b6:	48 89 e2             	mov    %rsp,%rdx
  4010b9:	48 83 e4 f0          	and    $0xfffffffffffffff0,%rsp
  4010bd:	50                   	push   %rax
  4010be:	54                   	push   %rsp
  4010bf:	49 c7 c0 b0 12 40 00 	mov    $0x4012b0,%r8
  4010c6:	48 c7 c1 40 12 40 00 	mov    $0x401240,%rcx
  4010cd:	48 c7 c7 82 11 40 00 	mov    $0x401182,%rdi
  4010d4:	e8 87 ff ff ff       	callq  401060 <__libc_start_main@plt>
  4010d9:	f4                   	hlt    
  4010da:	66 0f 1f 44 00 00    	nopw   0x0(%rax,%rax,1)

00000000004010e0 <deregister_tm_clones>:
  4010e0:	b8 60 40 40 00       	mov    $0x404060,%eax
  4010e5:	48 3d 60 40 40 00    	cmp    $0x404060,%rax
  4010eb:	74 13                	je     401100 <deregister_tm_clones+0x20>
  4010ed:	b8 00 00 00 00       	mov    $0x0,%eax
  4010f2:	48 85 c0             	test   %rax,%rax
  4010f5:	74 09                	je     401100 <deregister_tm_clones+0x20>
  4010f7:	bf 60 40 40 00       	mov    $0x404060,%edi
  4010fc:	ff e0                	jmpq   *%rax
  4010fe:	66 90                	xchg   %ax,%ax
  401100:	c3                   	retq   
  401101:	66 66 2e 0f 1f 84 00 	data16 nopw %cs:0x0(%rax,%rax,1)
  401108:	00 00 00 00 
  40110c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000401110 <register_tm_clones>:
  401110:	be 60 40 40 00       	mov    $0x404060,%esi
  401115:	48 81 ee 60 40 40 00 	sub    $0x404060,%rsi
  40111c:	48 89 f0             	mov    %rsi,%rax
  40111f:	48 c1 ee 3f          	shr    $0x3f,%rsi
  401123:	48 c1 f8 03          	sar    $0x3,%rax
  401127:	48 01 c6             	add    %rax,%rsi
  40112a:	48 d1 fe             	sar    %rsi
  40112d:	74 11                	je     401140 <register_tm_clones+0x30>
  40112f:	b8 00 00 00 00       	mov    $0x0,%eax
  401134:	48 85 c0             	test   %rax,%rax
  401137:	74 07                	je     401140 <register_tm_clones+0x30>
  401139:	bf 60 40 40 00       	mov    $0x404060,%edi
  40113e:	ff e0                	jmpq   *%rax
  401140:	c3                   	retq   
  401141:	66 66 2e 0f 1f 84 00 	data16 nopw %cs:0x0(%rax,%rax,1)
  401148:	00 00 00 00 
  40114c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000401150 <__do_global_dtors_aux>:
  401150:	80 3d 09 2f 00 00 00 	cmpb   $0x0,0x2f09(%rip)        # 404060 <__TMC_END__>
  401157:	75 17                	jne    401170 <__do_global_dtors_aux+0x20>
  401159:	55                   	push   %rbp
  40115a:	48 89 e5             	mov    %rsp,%rbp
  40115d:	e8 7e ff ff ff       	callq  4010e0 <deregister_tm_clones>
  401162:	c6 05 f7 2e 00 00 01 	movb   $0x1,0x2ef7(%rip)        # 404060 <__TMC_END__>
  401169:	5d                   	pop    %rbp
  40116a:	c3                   	retq   
  40116b:	0f 1f 44 00 00       	nopl   0x0(%rax,%rax,1)
  401170:	c3                   	retq   
  401171:	66 66 2e 0f 1f 84 00 	data16 nopw %cs:0x0(%rax,%rax,1)
  401178:	00 00 00 00 
  40117c:	0f 1f 40 00          	nopl   0x0(%rax)

0000000000401180 <frame_dummy>:
  401180:	eb 8e                	jmp    401110 <register_tm_clones>

0000000000401182 <main>:
  401182:	55                   	push   %rbp
  401183:	48 89 e5             	mov    %rsp,%rbp
  401186:	41 54                	push   %r12
  401188:	53                   	push   %rbx
  401189:	48 83 ec 10          	sub    $0x10,%rsp
  40118d:	e8 0e ff ff ff       	callq  4010a0 <fork@plt>
  401192:	89 45 ec             	mov    %eax,-0x14(%rbp)
  401195:	83 7d ec 00          	cmpl   $0x0,-0x14(%rbp)
  401199:	79 19                	jns    4011b4 <main+0x32>
  40119b:	b9 d6 20 40 00       	mov    $0x4020d6,%ecx
  4011a0:	ba 0a 00 00 00       	mov    $0xa,%edx
  4011a5:	be 10 20 40 00       	mov    $0x402010,%esi
  4011aa:	bf 19 20 40 00       	mov    $0x402019,%edi
  4011af:	e8 9c fe ff ff       	callq  401050 <__assert_fail@plt>
  4011b4:	83 7d ec 00          	cmpl   $0x0,-0x14(%rbp)
  4011b8:	75 45                	jne    4011ff <main+0x7d>
  4011ba:	8b 1d 9c 2e 00 00    	mov    0x2e9c(%rip),%ebx        # 40405c <g_value>
  4011c0:	e8 bb fe ff ff       	callq  401080 <getppid@plt>
  4011c5:	41 89 c4             	mov    %eax,%r12d
  4011c8:	e8 63 fe ff ff       	callq  401030 <getpid@plt>
  4011cd:	41 b8 5c 40 40 00    	mov    $0x40405c,%r8d
  4011d3:	89 d9                	mov    %ebx,%ecx
  4011d5:	44 89 e2             	mov    %r12d,%edx
  4011d8:	89 c6                	mov    %eax,%esi
  4011da:	bf 28 20 40 00       	mov    $0x402028,%edi
  4011df:	b8 00 00 00 00       	mov    $0x0,%eax
  4011e4:	e8 57 fe ff ff       	callq  401040 <printf@plt>
  4011e9:	bf 01 00 00 00       	mov    $0x1,%edi
  4011ee:	e8 9d fe ff ff       	callq  401090 <sleep@plt>
  4011f3:	c7 05 5f 2e 00 00 c8 	movl   $0xc8,0x2e5f(%rip)        # 40405c <g_value>
  4011fa:	00 00 00 
  4011fd:	eb bb                	jmp    4011ba <main+0x38>
  4011ff:	8b 1d 57 2e 00 00    	mov    0x2e57(%rip),%ebx        # 40405c <g_value>
  401205:	e8 76 fe ff ff       	callq  401080 <getppid@plt>
  40120a:	41 89 c4             	mov    %eax,%r12d
  40120d:	e8 1e fe ff ff       	callq  401030 <getpid@plt>
  401212:	41 b8 5c 40 40 00    	mov    $0x40405c,%r8d
  401218:	89 d9                	mov    %ebx,%ecx
  40121a:	44 89 e2             	mov    %r12d,%edx
  40121d:	89 c6                	mov    %eax,%esi
  40121f:	bf 80 20 40 00       	mov    $0x402080,%edi
  401224:	b8 00 00 00 00       	mov    $0x0,%eax
  401229:	e8 12 fe ff ff       	callq  401040 <printf@plt>
  40122e:	bf 01 00 00 00       	mov    $0x1,%edi
  401233:	e8 58 fe ff ff       	callq  401090 <sleep@plt>
  401238:	eb c5                	jmp    4011ff <main+0x7d>
  40123a:	66 0f 1f 44 00 00    	nopw   0x0(%rax,%rax,1)

0000000000401240 <__libc_csu_init>:
  401240:	41 57                	push   %r15
  401242:	41 89 ff             	mov    %edi,%r15d
  401245:	41 56                	push   %r14
  401247:	49 89 f6             	mov    %rsi,%r14
  40124a:	41 55                	push   %r13
  40124c:	49 89 d5             	mov    %rdx,%r13
  40124f:	41 54                	push   %r12
  401251:	4c 8d 25 c0 2b 00 00 	lea    0x2bc0(%rip),%r12        # 403e18 <__frame_dummy_init_array_entry>
  401258:	55                   	push   %rbp
  401259:	48 8d 2d c0 2b 00 00 	lea    0x2bc0(%rip),%rbp        # 403e20 <__init_array_end>
  401260:	53                   	push   %rbx
  401261:	4c 29 e5             	sub    %r12,%rbp
  401264:	31 db                	xor    %ebx,%ebx
  401266:	48 c1 fd 03          	sar    $0x3,%rbp
  40126a:	48 83 ec 08          	sub    $0x8,%rsp
  40126e:	e8 8d fd ff ff       	callq  401000 <_init>
  401273:	48 85 ed             	test   %rbp,%rbp
  401276:	74 1e                	je     401296 <__libc_csu_init+0x56>
  401278:	0f 1f 84 00 00 00 00 	nopl   0x0(%rax,%rax,1)
  40127f:	00 
  401280:	4c 89 ea             	mov    %r13,%rdx
  401283:	4c 89 f6             	mov    %r14,%rsi
  401286:	44 89 ff             	mov    %r15d,%edi
  401289:	41 ff 14 dc          	callq  *(%r12,%rbx,8)
  40128d:	48 83 c3 01          	add    $0x1,%rbx
  401291:	48 39 eb             	cmp    %rbp,%rbx
  401294:	75 ea                	jne    401280 <__libc_csu_init+0x40>
  401296:	48 83 c4 08          	add    $0x8,%rsp
  40129a:	5b                   	pop    %rbx
  40129b:	5d                   	pop    %rbp
  40129c:	41 5c                	pop    %r12
  40129e:	41 5d                	pop    %r13
  4012a0:	41 5e                	pop    %r14
  4012a2:	41 5f                	pop    %r15
  4012a4:	c3                   	retq   
  4012a5:	90                   	nop
  4012a6:	66 2e 0f 1f 84 00 00 	nopw   %cs:0x0(%rax,%rax,1)
  4012ad:	00 00 00 

00000000004012b0 <__libc_csu_fini>:
  4012b0:	f3 c3                	repz retq 

Disassembly of section .fini:

00000000004012b4 <_fini>:
  4012b4:	48 83 ec 08          	sub    $0x8,%rsp
  4012b8:	48 83 c4 08          	add    $0x8,%rsp
  4012bc:	c3                   	retq   
