#pragma once
#include <iostream>
#include <string>
#include <cassert>
#include <unordered_map>
#include <functional>
#include <ctime>
#include "Epoller.hpp"
#include "Sock.hpp"
#include "Log.hpp"
#include "Util.hpp"
#include "Protocol.hpp"

using namespace protocol_ns;

const static int gport = 8888;
const static int bsize = 1024;
const static int linkTimeout = 30;

class Connection;
class EpollServer;

using func_t = std::function<Response(const Request &)>;
using callback_t = std::function<void(Connection *)>;

// 大号的结构体
class Connection
{
public:
    Connection(const int &fd, const std::string &clientip, const uint16_t &clientport)
        : fd_(fd), clientip_(clientip), clientport_(clientport_)
    {
    }
    void Register(callback_t recver, callback_t sender, callback_t excepter)
    {
        recver_ = recver;
        sender_ = sender;
        excepter_ = excepter;
    }
    ~Connection()
    {
    }

public:
    // IO信息
    int fd_;
    std::string inbuffer_;
    std::string outbuffer_;
    // IO处理函数
    callback_t recver_;
    callback_t sender_;
    callback_t excepter_;

    // 用户信息, only debug
    std::string clientip_;
    uint16_t clientport_;

    // 也可以给conn带上自己要关心的事件
    uint32_t events;

    // 回指指针
    EpollServer *R;

    // 时间戳
    time_t lasttime; //该connection最近一次就绪的时间
};

// 1. Reactor
// 2. 扩展
class EpollServer
{
    const static int gnum = 64;

public:
    EpollServer(func_t func, uint16_t port = gport) : func_(func), port_(port)
    {
    }

    void InitServer()
    {
        listensock_.Socket();
        listensock_.Bind(port_);
        listensock_.Listen();
        epoller_.Create();
        AddConnection(listensock_.Fd(), EPOLLIN | EPOLLET);
        logMessage(Debug, "init server success");
    }

    // 事件派发器
    int Dispatcher() // 名字要改
    {
        int timeout = 1000;
        while (true)
        {
            LoopOnce(timeout); // timeout的时间应该设置成为多少？最小堆， 时间轮

            checkLink();
        }
    }

    void LoopOnce(int timeout)
    {
        int n = epoller_.Wait(revs_, gnum, timeout);
        for (int i = 0; i < n; i++)
        {
            int fd = revs_[i].data.fd;
            uint32_t events = revs_[i].events;
            logMessage(Debug, "当前正在处理%d上的%s", fd, (events & EPOLLIN) ? "EPOLLIN" : "OTHER");
            // 我们将所有的异常情况，最后全部转化成为recv，send的异常！
            if ((events & EPOLLERR) || (events & EPOLLHUP))
                events |= (EPOLLIN | EPOLLOUT);

            if ((events & EPOLLIN) && ConnIsExists(fd))
                connections_[fd]->recver_(connections_[fd]); 
            if ((events & EPOLLOUT) && ConnIsExists(fd))
                connections_[fd]->sender_(connections_[fd]);
        }
    }

    void AddConnection(int fd, uint32_t events, std::string ip = "127.0.0.1", uint16_t port = gport)
    {
        // 1. 设置fd是非阻塞
        if (events & EPOLLET)
            Util::SetNonBlock(fd);
        // 2. 构建connection对象，交给connections_来进行管理
        Connection *conn = new Connection(fd, ip, port);
        if (fd == listensock_.Fd())
        {
            conn->Register(std::bind(&EpollServer::Accepter, this, std::placeholders::_1), nullptr, nullptr);
        }
        else
        {
            conn->Register(std::bind(&EpollServer::Recver, this, std::placeholders::_1),
                           std::bind(&EpollServer::Sender, this, std::placeholders::_1),
                           std::bind(&EpollServer::Excepter, this, std::placeholders::_1));
        }
        conn->events = events;
        conn->R = this;
        conn->lasttime = time(nullptr);
        connections_.insert(std::pair<int, Connection *>(fd, conn));
        // 3. fd && events 写透到内核中
        bool r = epoller_.AddModEvent(fd, events, EPOLL_CTL_ADD);
        assert(r);
        (void)r;

        logMessage(Debug, "AddConnection success, fd: %d, clientinfo: [%s:%d]", fd, ip.c_str(), port);
    }
    bool EnableReadWrite(Connection *conn, bool readable, bool writeable)
    {
        conn->events = ((readable ? EPOLLIN : 0) | (writeable ? EPOLLOUT : 0) | EPOLLET);
        return epoller_.AddModEvent(conn->fd_, conn->events, EPOLL_CTL_MOD);
    }
    // 连接管理器
    void Accepter(Connection *conn)
    {
        do
        {
            int err = 0;
            // 1. 新连接到来
            // logMessage(Debug, "get a new link ...");
            std::string clientip;
            uint16_t clientport;
            int sock = listensock_.Accept(&clientip, &clientport, &err);
            if (sock > 0)
            {
                logMessage(Debug, "%s:%d 已经连上了服务器了", clientip.c_str(), clientport);
                // 1.1 此时在这里，我们能不能进行recv/read ? 不能，只有epoll知道sock上面的事件情况，将sock添加到epoll中
                AddConnection(sock, EPOLLIN | EPOLLET, clientip, clientport);
            }
            else
            {
                if (err == EAGAIN || err == EWOULDBLOCK)
                    break;
                else if (err == EINTR)
                    continue;
                else
                {
                    logMessage(Warning, "errstring : %s, errcode: %d", strerror(err), err);
                    continue;
                }
            }
        } while (conn->events & EPOLLET);

        logMessage(Debug, "accepter done ...");
    }
    void HandlerRequest(Connection *conn)
    {
        // 根据基本协议，进行数据分析 -- 自己定过一个！
        int quit = false;
        while (!quit)
        {
            std::string requestStr;
            // 1. 提取完整报文
            int n = protocol_ns::ParsePackage(conn->inbuffer_, &requestStr);
            if (n > 0)
            {
                // 2. 提取有效载荷
                requestStr = protocol_ns::RemoveHeader(requestStr, n);
                // 3. 进行反序列化
                Request req;
                req.Deserialize(requestStr);
                // 4. 进行业务处理
                Response resp = func_(req); // request 保证是一个完整的请求报文！
                // 5. 序列化
                std::string RespStr;
                resp.Serialize(&RespStr);
                // 6. 添加报头
                RespStr = AddHeader(RespStr);

                // 7. 进行返回
                conn->outbuffer_ += RespStr;
            }
            else
                quit = true;
        }
    }
    bool RecverHelper(Connection *conn)
    {
        int ret = true;
        conn->lasttime = time(nullptr); //更新conn最近访问时间
        // 读取完毕本轮数据！
        do
        {
            char buffer[bsize];
            ssize_t n = recv(conn->fd_, buffer, sizeof(buffer) - 1, 0);
            if (n > 0)
            {
                buffer[n] = 0;
                conn->inbuffer_ += buffer;
                // logMessage(Debug, "inbuffer: %s, [%d]", conn->inbuffer_.c_str(), conn->fd_);
            }
            else if (n == 0)
            {
                conn->excepter_(conn);
                ret = false;
                break;
            }
            else
            {
                if (errno == EAGAIN || errno == EWOULDBLOCK)
                    break;
                else if (errno == EINTR)
                    continue;
                else
                {
                    conn->excepter_(conn);
                    ret = false;

                    break;
                }
            }
        } while (conn->events & EPOLLET);

        return ret;
    }
    void Recver(Connection *conn)
    {
        if(!RecverHelper(conn)) return;
        HandlerRequest(conn);
        // 一般我们在面对写入的时候，直接写入，没写完，才交给epoll！
        if (!conn->outbuffer_.empty())
            conn->sender_(conn); // 只是第一次触发发送
    }

    void Sender(Connection *conn)
    {
        bool safe = true;
        do
        {
            ssize_t n = send(conn->fd_, conn->outbuffer_.c_str(), conn->outbuffer_.size(), 0);
            if (n > 0)
            {
                conn->outbuffer_.erase(0, n);
                if (conn->outbuffer_.empty())
                    break;
            }
            else
            {
                if (errno == EAGAIN || errno == EWOULDBLOCK)
                {
                    break;
                }
                else if (errno == EINTR)
                    continue;
                else
                {
                    safe = false;
                    conn->excepter_(conn);
                    break;
                }
            }
        } while (conn->events & EPOLLET);
        if (!safe)
            return;
        if (!conn->outbuffer_.empty())
            EnableReadWrite(conn, true, true);
        else
            EnableReadWrite(conn, true, false);
    }

    void Excepter(Connection *conn)
    {
        // 1. 先从epoll移除fd
        epoller_.DelEvent(conn->fd_);

        // 2. 移除unordered_map KV关系
        connections_.erase(conn->fd_);

        // 3. 关闭fd
        close(conn->fd_);

        // 5. conn对象释放掉
        delete conn;

        logMessage(Debug, "Excepter...done, fd: %d, clientinfo: [%s:%d]", conn->fd_, conn->clientip_.c_str(), conn->clientport_);
    }

    bool ConnIsExists(int fd)
    {
        return connections_.find(fd) != connections_.end();
    }

    void checkLink()
    {
        // time_t curr = time(nullptr);
        // for(auto &connection : connections_)
        // {
        //     if(connection.second->lasttime + linkTimeout > curr) continue;
        //     else Excepter(connection.second);
        // }
    }

    ~EpollServer()
    {
        listensock_.Close();
        epoller_.Close();
    }

private:
    uint16_t port_;
    Sock listensock_;
    Epoller epoller_;
    struct epoll_event revs_[gnum];
    func_t func_;
    std::unordered_map<int, Connection *> connections_;
};