#pragma once

#include <iostream>
#include <string>
#include <stdlib.h>
#include <sys/epoll.h>
#include "Log.hpp"
#include "Err.hpp"

static const int defaultepfd = -1;
static const int gsize = 128;

class Epoller
{
public:
    Epoller() : epfd_(defaultepfd)
    {
    }
    void Create()
    {
        epfd_ = epoll_create(gsize);
        if (epfd_ < 0)
        {
            logMessage(Fatal, "epoll_create error, code: %d, errstring: %s", errno, strerror(errno));
            exit(EPOLL_CREAT_ERR);
        }
    }
    // 用户 -> 内核
    bool AddEvent(int fd, uint32_t events)
    {
        struct epoll_event ev;
        ev.events = events;
        ev.data.fd = fd;             //用户数据, epoll底层不对该数据做任何修改，就是为了给未来就绪返回的！
        int n = epoll_ctl(epfd_, EPOLL_CTL_ADD, fd, &ev);
        if(n < 0)
        {
            logMessage(Fatal, "epoll_ctl error, code: %d, errstring: %s", errno, strerror(errno));
            return false;
        }
        return true;
    }
    bool DelEvent(int fd)
    {
        // epoll在操作fd的时候，有一个要求，fd必须合法！
        return epoll_ctl(epfd_, EPOLL_CTL_DEL, fd, nullptr) == 0;
    }
    int Wait(struct epoll_event *revs, int num, int timeout)
    {
        // 
        return epoll_wait(epfd_, revs, num, timeout);
    }
    int Fd()
    {
        return epfd_;
    }
    void Close()
    {
        if(epfd_ != defaultepfd) close(epfd_); 
    }
    ~Epoller()
    {
    }

private:
    int epfd_;
};