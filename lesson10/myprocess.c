#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <sys/types.h>

int main()
{
    int x = 100;
    pid_t ret = fork();
    //assert(ret != -1);
    if(ret == 0)
    {
        //子进程
        while(1)
        {
            printf("我是子进程, 我的pid是: %d, 我的父进程是: %d, %d, %p\n", getpid(), getppid(), x, &x);
            sleep(1);
        }
    }
    else if(ret > 0)
    {
        //父进程
        while(1)
        {
            printf("我是父进程, 我的pid是: %d, 我的父进程是: %d, %d, %p\n", getpid(), getppid(), x, &x);
            x = 4321;
            sleep(1);
        }
    }
    else
    {}

//    while(1)
//    {
//        printf("hello proccess, 我已经是一个进程了，我的pid是: %d, 我的父进程是: %d\n", getpid(), getppid());
//        sleep(1);
//    }
//
    return 0;
}
