#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>
#include <unistd.h>
#include <pthread.h>

using namespace std;

int tickets = 10000; // 加锁保证共享资源的安全
pthread_mutex_t mutex; // 后面说

void *threadRoutine(void *name)
{
    string tname = static_cast<const char*>(name);

    while(true)
    {
        pthread_mutex_lock(&mutex); // 所有线程都要遵守这个规则
        if(tickets > 0) // tickets == 1; a, b, c,d
        {
            //a,b,c,d
            usleep(2000); // 模拟抢票花费的时间
            cout << tname << " get a ticket: " << tickets-- << endl;
            pthread_mutex_unlock(&mutex);
        }
        else
        {
            pthread_mutex_unlock(&mutex);
        }

        // 后面还有动作
        usleep(1000); //充当抢完一张票，后续动作
    }

    return nullptr;
}

int main()
{
    pthread_mutex_init(&mutex, nullptr);

    pthread_t t[4];
    int n = sizeof(t)/sizeof(t[0]);
    for(int i = 0; i < n; i++)
    {
        char *data = new char[64];
        snprintf(data, 64, "thread-%d", i+1);
        pthread_create(t+i, nullptr, threadRoutine, data);
    }

    for(int i = 0; i < n; i++)
    {
        pthread_join(t[i], nullptr);
    }

    pthread_mutex_destroy(&mutex);
    return 0;
}
















// // __thread int g_val = 100;

// int g_val = 100;

// std::string hexAddr(pthread_t tid)
// {
//     g_val++;
//     char buffer[64];
//     snprintf(buffer, sizeof(buffer), "0x%x", tid);

//     return buffer;
// }

// void *threadRoutine(void* args)
// {
//     // static int a = 10;
//     string name = static_cast<const char*>(args);
//     int cnt = 5;
//     while(cnt)
//     {
//         // cout << name << " : " << cnt-- << " : " << hexAddr(pthread_self()) << " &cnt: " << &cnt << endl;
//         cout << name << " g_val: " << g_val++ << ", &g_val: " << &g_val << endl;
//         sleep(1);
//     }
//     return nullptr;
// }

// int main()
// {
//     pthread_t t1, t2, t3;
//     pthread_create(&t1, nullptr, threadRoutine, (void*)"thread 1"); // 线程被创建的时候，谁先执行不确定！
//     pthread_create(&t2, nullptr, threadRoutine, (void*)"thread 2"); // 线程被创建的时候，谁先执行不确定！
//     pthread_create(&t3, nullptr, threadRoutine, (void*)"thread 3"); // 线程被创建的时候，谁先执行不确定!

//     pthread_join(t1, nullptr);
//     pthread_join(t2, nullptr);
//     pthread_join(t3, nullptr);

//     // pthread_detach(tid);

//     // while(true)
//     // {
//     //     cout << " main thread:  " << hexAddr(pthread_self()) << " new thread id: " << hexAddr(tid) << endl;
//     //     sleep(1);
//     // }

//     // if( 0 != n )
//     // {
//     //     std::cerr << "error: " << n << " : " <<  strerror(n) << std::endl;
//     // }

//     // sleep(10);
//     return 0;
// }


// void run1()
// {
//     while(true)
//     {
//         cout << "thread 1" << endl;
//         sleep(1);
//     }
// }
// void run2()
// {
//     while(true)
//     {
//         cout << "thread 2" << endl;
//         sleep(1);
//     }
// }
// void run3()
// {
//     while(true)
//     {
//         cout << "thread 3" << endl;
//         sleep(1);
//     }
// }


// int main()
// {
//     thread th1(run1);
//     thread th2(run2);
//     thread th3(run3);

//     th1.join();
//     th2.join();
//     th3.join();

//     return 0;
// }
