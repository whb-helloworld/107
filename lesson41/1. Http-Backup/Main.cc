#include "HttpServer.hpp"
#include <memory>

std::string HandlerHttp(const std::string &request)
{
    // 确信，request一定是一个完整的http请求报文
    // 给别人返回的是一个http response

    return "";
}

int main()
{
    uint16_t port = 8888;
    std::unique_ptr<HttpServer> tsvr(new HttpServer(HandlerHttp, port)); // TODO
    tsvr->InitServer();
    tsvr->Start();

    return 0;
}